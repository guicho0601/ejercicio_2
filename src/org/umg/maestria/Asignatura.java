package org.umg.maestria;

/**
 * Created by Luis on 3/07/17.
 */
public class Asignatura {

    private final String nombre;
    private final int nota, cantEvaluaciones;

    public Asignatura(String nombre, int nota, int cantEvaluaciones) {
        this.nombre = nombre;
        this.nota = nota;
        this.cantEvaluaciones = cantEvaluaciones;
    }

    public String getNombre() {
        return nombre;
    }

    public int getNota() {
        return nota;
    }

    public int getCantEvaluaciones() {
        return cantEvaluaciones;
    }
}
