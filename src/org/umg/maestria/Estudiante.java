package org.umg.maestria;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis on 3/07/17.
 */
public class Estudiante {

    private final String nombre;
    private final int edad;
    private final List<Asignatura> asignaturas;

    public Estudiante(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
        this.asignaturas = new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public int getEdad() {
        return edad;
    }

    public List<Asignatura> getAsignaturas() {
        return asignaturas;
    }

    public void agregarAsignatura(Asignatura asignatura) {
        this.asignaturas.add(asignatura);
    }

    public void eliminarAsignatura(int posicion) {
        this.asignaturas.remove(posicion);
    }

    public float getPromedio() {
        if (this.asignaturas.size() == 0)
            return 0;
        int suma = 0;
        for (Asignatura a : this.asignaturas) {
            suma += a.getNota();
        }
        return suma / this.asignaturas.size();
    }
}
