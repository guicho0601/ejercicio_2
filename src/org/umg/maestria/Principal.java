package org.umg.maestria;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Luis on 3/07/17.
 */
public class Principal {

    public int seleccionarEstuidante(List<Estudiante> estudiantes){
        System.out.println("-----------------------");
        System.out.println("Seleccione un estudiante:");
        for(int i = 0; i < estudiantes.size(); i++){
            System.out.println((i+1) + ". " + estudiantes.get(i).getNombre());
        }
        return new Scanner(System.in).nextInt() - 1;
    }

    public int seleccionarProfesor(List<Profesor> profesores){
        System.out.println("-----------------------");
        System.out.println("Seleccione un profesor:");
        for(int i = 0; i < profesores.size(); i++){
            System.out.println((i+1) + ". " + profesores.get(i).getNombre());
        }
        return new Scanner(System.in).nextInt() - 1;
    }

    public static void main(String[] args) {
        Scanner sn = new Scanner(System.in);
        boolean salir = false;
        int opcion;
        List<Profesor> profesores = new ArrayList<>();
        Principal principal = new Principal();

        while (!salir) {
            System.out.println("Sistema de Asignaturas");
            System.out.println("-----------------------");
            System.out.println("1. Agregar profesor");
            System.out.println("2. Agregar alumno");
            System.out.println("3. Agregar asignatura");
            System.out.println("4. Promedio de estudiante");
            System.out.println("5. Mejor estudiante");
            System.out.println("0. Salir");


            try {
                System.out.println("Seleccione una de las opciones");
                opcion = sn.nextInt();

                switch (opcion) {
                    case 1:
                        System.out.println("Nombre:");
                        sn.nextLine();
                        String nombre = sn.nextLine();
                        System.out.println("Especialidad:");
                        String especialidad = sn.nextLine();
                        profesores.add(new Profesor(nombre, especialidad));
                        break;
                    case 2:
                        int p = principal.seleccionarProfesor(profesores);
                        sn.nextLine();
                        System.out.println("Nombre del estudiante:");
                        String n = sn.nextLine();
                        System.out.println("Edad del estudiante:");
                        int e = sn.nextInt();
                        profesores.get(p).agregarEstudiante(new Estudiante(n, e));
                        break;
                    case 3:
                        int o = principal.seleccionarProfesor(profesores);
                        int q = principal.seleccionarEstuidante(profesores.get(o).getEstudiantes());
                        sn.nextLine();
                        System.out.println("Nombre de la asignatura:");
                        String nombre_asignatura = sn.nextLine();
                        System.out.println("Nota del estudiante:");
                        int nota = sn.nextInt();
                        System.out.println("Cantidad de evaluaciones:");
                        int cantidad = sn.nextInt();
                        profesores.get(o).getEstudiantes().get(q).agregarAsignatura(new Asignatura(nombre_asignatura, nota, cantidad));
                        break;
                    case 4:
                        int r = principal.seleccionarProfesor(profesores);
                        int s = principal.seleccionarEstuidante(profesores.get(r).getEstudiantes());
                        Estudiante estudiante = profesores.get(r).getEstudiantes().get(s);
                        System.out.println("El getPromedio del estudiante " + estudiante.getNombre() + " es de: " + estudiante.getPromedio());
                        break;
                    case 5:
                        int t = principal.seleccionarProfesor(profesores);
                        Estudiante estudiante1 = profesores.get(t).mejorEstudiante();
                        System.out.println("El mejor estudiante es: " + estudiante1.getNombre());
                        break;
                    case 0:
                        salir = true;
                        break;
                    default:
                        System.out.println("Solo números entre 1 y 5");
                }
                System.out.println("--------------");
            } catch (InputMismatchException e) {
                System.out.println("Debes insertar un número");
                sn.next();
            }
        }
    }
}
