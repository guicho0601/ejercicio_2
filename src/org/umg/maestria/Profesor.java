package org.umg.maestria;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis on 3/07/17.
 */
public class Profesor {

    private final String nombre, especialidad;
    private final List<Estudiante> estudiantes;

    public Profesor(String nombre, String especialidad) {
        this.nombre = nombre;
        this.especialidad = especialidad;
        this.estudiantes = new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public List<Estudiante> getEstudiantes() {
        return estudiantes;
    }

    public void agregarEstudiante(Estudiante e){
        this.estudiantes.add(e);
    }

    public Estudiante mejorEstudiante(){
        Estudiante estudiante = new Estudiante("Ninguno", 0);
        for (Estudiante e : this.estudiantes){
            if(e.getPromedio() > estudiante.getPromedio())
                estudiante = e;
        }
        return  estudiante;
    }

    public float getTotal(){
        float suma = 0;
        for(Estudiante e : this.estudiantes)
            suma += e.getPromedio();
        return suma;
    }
}
