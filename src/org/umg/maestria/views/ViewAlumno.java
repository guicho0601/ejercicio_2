package org.umg.maestria.views;

import org.umg.maestria.Estudiante;
import org.umg.maestria.Profesor;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;

public class ViewAlumno extends JDialog {
    private JPanel contentPane;
    private JTextField nombreTextField;
    private JTextField edadTextField;
    private JButton agregarButton;
    private JTable table1;
    private JButton asignaturasButton;
    private JButton eliminarButton;
    private JLabel estudianteDestacadoLabel;
    private JLabel totalLabel;
    private DefaultTableModel tableModel;
    private Profesor profesor;

    public ViewAlumno() {
        setContentPane(contentPane);
        setModal(true);

        profesor = new Profesor("LM", "Programacion");

        tableModel = new DefaultTableModel();

        tableModel.addColumn("Nombre");
        tableModel.addColumn("Edad");
        tableModel.addColumn("Promedio");

        table1.setModel(tableModel);

        agregarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                agregarEstudiante();
            }
        });
        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarEstudiante();
            }
        });
        asignaturasButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                abrirAsignaturas();
            }
        });
    }

    private void agregarEstudiante() {
        if (nombreTextField.getText().equals("") || edadTextField.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Se debe de ingresar nombre y edad.");
        } else {
            Estudiante estudiante = new Estudiante(nombreTextField.getText(), Integer.parseInt(edadTextField.getText()));
            profesor.agregarEstudiante(estudiante);
            tableModel.addRow(new Object[]{ estudiante.getNombre(), estudiante.getEdad(), estudiante.getPromedio() });
            this.limpiarCampos();
            this.mejoresEstudiantes();
        }
    }

    private void eliminarEstudiante() {
        if(table1.getSelectedRow() > -1){
            profesor.getEstudiantes().remove(table1.getSelectedRow());
            tableModel.removeRow(table1.getSelectedRow());
            this.mejoresEstudiantes();
        }
    }

    private void abrirAsignaturas() {
        if(table1.getSelectedRow() > -1){
            Estudiante estudiante = profesor.getEstudiantes().get(table1.getSelectedRow());
            ViewAsignaturas dialog = new ViewAsignaturas(this, estudiante);
            dialog.pack();
            dialog.setVisible(true);
        }
    }

    public void cerrarAsignatura(Estudiante estudiante){
        profesor.getEstudiantes().set(table1.getSelectedRow(), estudiante);
        tableModel.setValueAt(estudiante.getPromedio(), table1.getSelectedRow(), 2);
        this.mejoresEstudiantes();
    }

    private void limpiarCampos(){
        nombreTextField.setText("");
        edadTextField.setText("");
    }

    private void mejoresEstudiantes(){
        totalLabel.setText("Total: " + profesor.getTotal());
        estudianteDestacadoLabel.setText("Estudiante destacado: " + profesor.mejorEstudiante().getNombre());
    }

    public static void main(String[] args) {
        ViewAlumno dialog = new ViewAlumno();
        dialog.setName("UMG");
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
