package org.umg.maestria.views;

import org.umg.maestria.Asignatura;
import org.umg.maestria.Estudiante;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;

public class ViewAsignaturas extends JDialog {
    private JPanel contentPane;
    private JLabel alumnoLabel;
    private JTextField asignaturaTextField;
    private JTextField notaTextField;
    private JTextField cantidadTextField;
    private JButton agregarButton;
    private JButton eliminarButton;
    private JTable table1;
    private JButton regresarButton;
    private final Estudiante estudiante;
    private DefaultTableModel tableModel;
    private final ViewAlumno viewViewAlumno;

    public ViewAsignaturas(ViewAlumno viewViewAlumno, Estudiante estudiante) {
        setContentPane(contentPane);
        setModal(true);
        this.estudiante = estudiante;
        this.viewViewAlumno = viewViewAlumno;

        tableModel = new DefaultTableModel();

        tableModel.addColumn("Nombre asignatura");
        tableModel.addColumn("Nota");
        tableModel.addColumn("Cantidad de evaluaciones");

        table1.setModel(tableModel);

        for (Asignatura a : estudiante.getAsignaturas()){
            tableModel.addRow(new Object[]{ a.getNombre(), a.getNota(), a.getCantEvaluaciones() });
        }

        alumnoLabel.setText("ViewAlumno: " + estudiante.getNombre());

        agregarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                agregarAsignatura();
            }
        });
        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarAsignatura();
            }
        });
        regresarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                regresar();
            }
        });
    }

    private void agregarAsignatura() {
        if (asignaturaTextField.getText().equals("") || notaTextField.getText().equals("") || cantidadTextField.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Debe de llenar los campos de asignatura, nota y cantidad de evaluaciones.");
        } else {
            Asignatura asignatura = new Asignatura(asignaturaTextField.getText(), Integer.parseInt(notaTextField.getText()), Integer.parseInt(cantidadTextField.getText()));
            tableModel.addRow(new Object[]{ asignatura.getNombre(), asignatura.getNota(), asignatura.getCantEvaluaciones() });
            estudiante.agregarAsignatura(asignatura);
            this.limpiarCampos();
        }
    }

    private void eliminarAsignatura() {
        if(table1.getSelectedRow() > -1){
            estudiante.eliminarAsignatura(table1.getSelectedRow());
            tableModel.removeRow(table1.getSelectedRow());
        }
    }

    private void regresar(){
        this.viewViewAlumno.cerrarAsignatura(this.estudiante);
        this.setVisible(false);
    }

    private void limpiarCampos() {
        asignaturaTextField.setText("");
        notaTextField.setText("");
        cantidadTextField.setText("");
    }
}
